# ADAPT Post Processing

This repository contains functionality for processing ADAPT-collected data after
a flight. Over time, these capabilities will be integrated into the real-time
processing deployed on board the ADAPT payload.

# License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
