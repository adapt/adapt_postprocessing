# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

NAME=adapt_postprocessing
TAG="latest"
IMAGENAME="${SERVER}${NAME}:${TAG}"

container_name=adapt_pykernel

# Ipython kernel name.
kernel_name=remote_kernel_adapt_postprocessing_pykernel.json

# Location on host to save remote Ipython kernels to.
host_kernel_dir=$(jupyter --runtime-dir)
#host_kernel_dir=/run/user/1000/jupyter/

# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $DIR/utilities.sh

# Open up xhost (for display) only to the container.
xhost +local:`docker inspect --format='{{ .Config.Hostname }}' $container_name`

# If the container does not exist, start it.
echo "Starting new IPython kernel container"

HOST_UID=$(echo $UID)
HOST_GID=$(id -g)

# Maping /tmp/.X11-unix allows graphics to be passed.
docker run --rm -it \
  --network="host" \
  --gpus all \
	-e "DISPLAY" \
 	-e "QT_X11_NO_MITSHM=1" \
  --user $(id -u):$(id -g) \
  -v "/tmp/.X11-unix:/tmp/.X11-unix" \
  -v "/etc/group:/etc/group:ro" \
  -v "/etc/passwd:/etc/passwd:ro" \
  -v "/etc/shadow:/etc/shadow:ro" \
  -v "$HOME/.config:$HOME/.config" \
  -v "$host_kernel_dir:$host_kernel_dir" \
  -v "$DIR/../:/home/user/adapt_postprocessing" \
  -v "/media:/media" \
  --privileged \
  --name $container_name \
  $IMAGENAME \
  /bin/bash -c "python -m spyder_kernels.console --pylab=auto --matplotlib=auto -f $kernel_name"

# python -m spyder_kernels.console --pylab=auto --matplotlib=auto -f $kernel_name
# python -m spyder_kernels.console --pylab=auto --matplotlib=auto -f remote_kernel_adapt_postprocessing_pykernel.json

# Use command '%matplotlib auto' within the remote console so that plots are not
# inline.

remove_container $container_name
