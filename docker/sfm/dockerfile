FROM ubuntu:18.04

# install packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    wget \
    curl \
    git \
    sudo \
    ca-certificates \
    libgtk2.0-0 \
    libcanberra-gtk-module \
    bzip2 \
    libx11-6 \
   	libxxf86vm1 \
    build-essential \
    libgl1-mesa-dev xvfb \
    libjpeg-dev \
    libpng-dev \
    libtiff-dev \
    libtbb-dev \
    cmake \
    qt5-default \
    && rm -rf /var/lib/apt/lists/*

# Create a non-root user and switch to it. Running X11 applications as root does
# not always work.
RUN adduser --disabled-password --gecos '' --shell /bin/bash user
RUN echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-user

WORKDIR /home/user

# MVS-Texturing
RUN git clone https://github.com/nmoehrle/mvs-texturing.git

COPY CMakeLists.txt /home/user/mvs-texturing/elibs/CMakeLists.txt

RUN cd /home/user/mvs-texturing \
  && mkdir build && cd build && cmake .. \
  && make -j

RUN apt-get update && apt-get install -y --no-install-recommends \
    libqt5opengl5-dev \
    && rm -rf /var/lib/apt/lists/*

# MVE
RUN git clone https://github.com/simonfuhrmann/mve.git \
  && cd mve \
  && make -j \
  && cd apps/umve/ \
  && qmake && make -j8
