# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


function remove_container()
{
  container_name=$1

  if ! [ -z `docker ps -a -q -f name=^/${container_name}$` ]; then
    docker stop $container_name
  fi

  if ! [ -z `docker ps -q -f name=${container_name}` ]; then
    docker rm $container_name
  fi
}


function start_container()
{
  image_name=$1
  container_name=$2

  if [ -z `docker ps -a -q -f name=^/${container_name}$` ]; then
    # If the container does not exist, start it.
    echo "Starting new container"

    # Open up xhost (for display) only to the container.
    xhost +local:`docker inspect --format='{{ .Config.Hostname }}' $container_name`

    HOST_UID=$(echo $UID)
    HOST_GID=$(id -g)

    # Maping /tmp/.X11-unix allows graphics to be passed.
    docker run --rm -dt \
      --gpus all \
      --network="host" \
    	-e "DISPLAY" \
      -e "SDL_VIDEODRIVER" \
      -e="QT_X11_NO_MITSHM=1" \
      -v "/tmp/.X11-unix:/tmp/.X11-unix" \
      --user $(id -u):$(id -g) \
      -v="$XAUTH:$XAUTH" \
      -v "/etc/group:/etc/group:ro" \
      -v "/etc/passwd:/etc/passwd:ro" \
      -v "/etc/shadow:/etc/shadow:ro" \
      -v "$HOME/.config:$HOME/.config" \
      -v "$HOME/.local:$HOME/.local" \
      -v "$DIR/../:/home/user/adapt_postprocessing" \
      -v "/media:/media" \
      --privileged \
      --name $container_name \
      $image_name \
      /bin/bash
  elif [ -z `docker ps -q -f name=${container_name}` ]; then
    # If the container exists but is stopped, start it.
    echo "Container is stopped, restarting it"
    docker start $container_name
  else
    echo "Container already running"
  fi
}
