NAME=adapt_postprocessing
TAG="latest"
IMAGENAME="${NAME}:${TAG}"
CONTAINER_NAME="${NAME}"

# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $DIR/utilities.sh

start_container $IMAGENAME $CONTAINER_NAME

docker exec -it \
    -e "DISPLAY" \
   	-e "QT_X11_NO_MITSHM=1" \
    $container_name \
    /bin/bash

remove_container $container_name
