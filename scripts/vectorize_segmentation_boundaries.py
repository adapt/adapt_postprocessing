#! /usr/bin/python
from __future__ import division, print_function
import numpy as np
import os
import cv2
import matplotlib.pyplot as plt
import glob
from osgeo import osr, gdal
from scipy.optimize import fmin, minimize, fminbound
import kwcoco
import kwimage
import sys
from math import sqrt


def human_reasable_size(size, precision=2):
    suffixes=['B','KB','MB','GB','TB']
    suffixIndex = 0
    while size > 1024 and suffixIndex < 4:
        suffixIndex += 1 #increment the index of the suffix
        size = size/1024.0 #apply the division
    return "%.*f%s"%(precision,size,suffixes[suffixIndex])


def get_size_in_bytes(obj, seen=None):
    """Recursively finds size of objects in bytes"""
    size = sys.getsizeof(obj)
    if seen is None:
        seen = set()
    obj_id = id(obj)
    if obj_id in seen:
        return 0
    # Important mark as seen *before* entering recursion to gracefully handle
    # self-referential objects
    seen.add(obj_id)
    if hasattr(obj, '__dict__'):
        for cls in obj.__class__.__mro__:
            if '__dict__' in cls.__dict__:
                d = cls.__dict__['__dict__']
                if inspect.isgetsetdescriptor(d) or inspect.ismemberdescriptor(d):
                    size += get_size_in_bytes(obj.__dict__, seen)
                break
    if isinstance(obj, dict):
        size += sum((get_size_in_bytes(v, seen) for v in obj.values()))
        size += sum((get_size_in_bytes(k, seen) for k in obj.keys()))
    elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
        size += sum((get_size_in_bytes(i, seen) for i in obj))

    return size


def ramer_douglas_peucker(points, epsilon):
    """Reduce number of points in curve using Ramer-Douglas-Peucker algorithm.

    :param points: Array of points.
    :param points: array-like: shape (N,2)

    :param epsilon: Threshold distance.
    :type epsilon: float

    """
    def point_line_distance(pt, s, e):
        d = sqrt((e[0] - s[0]) ** 2 + (e[1] - s[1]) ** 2)
        if d == 0:
            return 0
        else:
            n = abs((e[0] - s[0])*(s[1] - pt[1]) - (s[0] - pt[0])*(e[1] - s[1]))
            return n / d

    def rdp(points, epsilon):
        dmax = 0.0
        index = 0
        for i in range(1, len(points) - 1):
            d = point_line_distance(points[i], points[0], points[-1])
            if d > dmax:
                index = i
                dmax = d

        if dmax >= epsilon:
            results = rdp(points[:index+1], epsilon)[:-1]
            results = results + rdp(points[index:], epsilon)
        else:
            results = [points[0], points[-1]]

        return results

    points = np.array(points)
    points.shape = (-1,2)

    return np.array(rdp(points, epsilon))


# ----------------------------------------------------------------------------
seg_glob = '/media/mattb/7e7167ba-ad6f-4720-9ced-b699f49ba3aa/noaa_adapt/adam_500sq_task1-5_ignore-mix/Circle-sUAS-Visible-Responder-2017_05_08_noon_flight/segmentation_masks/*.png'

seg_fnames = glob.glob(seg_glob)

epsilon = 1

total_bytes = 0
for seg_fname in seg_fnames:
    seg_mask = cv2.imread(seg_fname, 0)
    seg_mask2 = np.zeros_like(seg_mask)

    obj_idxs = np.setdiff1d(np.unique(seg_mask), [0])


    for obj_idx in obj_idxs:
        bin_data = (seg_mask == obj_idx).astype(np.uint8)
        bin_mask = kwimage.Mask(bin_data, format='c_mask')

        bin_poly = bin_mask.to_multi_polygon()

        for poly1 in bin_poly:
            points = poly1.data['exterior'].data
            points = ramer_douglas_peucker(points, epsilon)
            poly1.data['exterior'].data = points


        for poly2 in poly1.data['interiors']:
            points = poly2.data
            points = ramer_douglas_peucker(points, epsilon)
            poly2.data = points

        bin_mask2 = bin_poly.to_mask(seg_mask.shape).data
        seg_mask2[bin_mask2 > 0] = obj_idx

        coco_ssegs = bin_poly.to_coco(style='new')

        for coco_sseg in coco_ssegs:
            total_bytes += 2*len(np.array(coco_sseg['exterior'],
                                          dtype=np.uint16).ravel())

            for p in coco_sseg['interiors']:
                total_bytes += 2*len(np.array(p, dtype=np.uint16).ravel())

print(total_bytes / len(seg_fnames))
