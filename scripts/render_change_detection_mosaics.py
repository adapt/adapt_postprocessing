#! /usr/bin/python
from __future__ import division, print_function
import numpy as np
import os
import cv2
import matplotlib.pyplot as plt
import glob
from osgeo import osr, gdal
from scipy.optimize import fmin, minimize, fminbound
import transformations

# Colmap Processing imports.
import colmap_processing.vtk_util as vtk_util
from colmap_processing.geo_conversions import llh_to_enu
from colmap_processing.colmap_interface import read_images_binary, Image, \
    read_points3d_binary, read_cameras_binary, qvec2rotmat
from colmap_processing.camera_models import StandardCamera
from colmap_processing.platform_pose import PlatformPoseInterp
from colmap_processing.world_models import WorldModelMesh


# ----------------------------------------------------------------------------
# Define the directory where all of the relavant COLMAP files are saved.
# If you are placing your data within the 'data' folder of this repository,
# this will be mapped to '/home_user/adapt_postprocessing/data' inside the
# Docker container.
data_dir = '/media/mattb/7e7167ba-ad6f-4720-9ced-b699f49ba3aa/noaa_adapt/change_detection'

image_subdirs = ['Circle-sUAS-Visible-Responder-2017_05_05_afternoon_flight',
                 'Circle-sUAS-Visible-Responder-2017_05_08_noon_flight']

# COLMAP data directory.
images_bin_fname = '%s/images.bin' % data_dir
camera_bin_fname = '%s/cameras.bin' % data_dir

# Ground mesh filename.
#mesh_fname = '%s/uav_circle.ply' % data_dir
mesh_fname = '/home/mattb/3d_models/uav_circle.ply'

# VTK renderings are limited to monitor resolution (width x height).
monitor_resolution = (2200, 1200)
# ----------------------------------------------------------------------------


# Read model into VTK.
model_reader = vtk_util.load_world_model(mesh_fname)
world_model = WorldModelMesh(mesh_fname)

# Read in the details of all images.
images = read_images_binary(images_bin_fname)
cameras = read_cameras_binary(camera_bin_fname)

mosaic_ind = {image_subdirs[i]:i for i in range(len(image_subdirs))}

# ---------------- Define a Camera Model for Each Camera ----------------------

# Use the calibrate calibration and structure from motion results from the
# Colmap database to define the camera models and their pose for each image.

# Pretend image index is the time.
platform_pose_provider = PlatformPoseInterp()
for image_id in images:
    image = images[image_id]

    R = qvec2rotmat(image.qvec)
    pos = -np.dot(R.T, image.tvec)

    # The qvec used by Colmap is a (w, x, y, z) quaternion representing the
    # rotation of a vector defined in the world coordinate system into the
    # camera coordinate system. However, the 'camera_models' module assumes
    # (x, y, z, w) quaternions representing a coordinate system rotation.
    quat = transformations.quaternion_inverse(image.qvec)
    quat = [quat[1], quat[2], quat[3], quat[0]]

    t = image_id
    platform_pose_provider.add_to_pose_time_series(t, pos, quat)

std_cams = {}
for camera_id in set([images[image_id].camera_id for image_id in images]):
    colmap_camera = cameras[camera_id]

    if colmap_camera.model == 'OPENCV':
        fx, fy, cx, cy, d1, d2, d3, d4 = colmap_camera.params

    K = K = np.array([[fx, 0, cx], [0, fy, cy], [0, 0, 1]])
    dist = np.array([d1, d2, d3, d4])

    # StandardCamera has specification for how the camera sits relative to an
    # interial navigation system. But, Colmap provides the camera pose directly,
    # so we set the camera pose relative to the pose provider as identity
    # operations.
    std_cams[camera_id] = StandardCamera(colmap_camera.width,
                                         colmap_camera.height, K, dist,
                                         [0, 0, 0], [0, 0, 0, 1],
                                         platform_pose_provider)
# -----------------------------------------------------------------------------


def read_image(image_id):
    fname = '%s/%s' % (data_dir, images[image_id].name)
    image = cv2.imread(fname)
    if image.ndim == 3:
        image = image[:, :, ::-1]

    return image


# Focus on specific image.
image_ids = sorted(images.keys())
image_names = [images[image_id].name for image_id in images]

image_id0 = image_ids[image_names.index('Circle-sUAS-Visible-Responder-2017_05_05_afternoon_flight/DSC00135.JPG')]
day1_image_ids = [i for i in image_ids if image_subdirs[0] in images[i].name]
day2_image_ids = [i for i in image_ids if image_subdirs[1] in images[i].name]


# Map mesh on image to world coordinates.
image0 = images[image_id0]
cm0 = std_cams[image0.camera_id]


# We used image ID as a proxy for image time. There is a state exactly on
# each image_id "time," so no spurious interpolation is done.
P = cm0.get_camera_pose(image_id0)
R = P[:3, :3]
t = P[:, 3]
pos = -np.dot(R.T, t)

K = cm0.K.copy()
K[0, 0] /= 4
K[1, 1] /= 4

img, depth, X, Y, Z = vtk_util.render_distored_image(cm0.width, cm0.height,
                                                     K, np.zeros(5), pos,
                                                     R, model_reader,
                                                     return_depth=True,
                                                     monitor_resolution=monitor_resolution,
                                                     clipping_range=[0.01, 2000])

gsd = np.mean(np.sqrt(np.diff(X)**2 + np.diff(Y)**2))

# Center of frame.
xyz_center = np.array([X[cm0.height//2, cm0.width//2],
                       Y[cm0.height//2, cm0.width//2],
                       Z[cm0.height//2, cm0.width//2]])

d = []
for image_id in other_image_ids:
    image = images[image_id]
    cm = std_cams[image.camera_id]

    ray_pos, ray_dir = cm.unproject([cm.width/2, cm.height/2], t=image_id)
    ip = world_model.intersect_rays(ray_pos, ray_dir).ravel()
    d.append(np.sqrt(np.sum((xyz_center - ip)**2)))

other_image_ids = [other_image_ids[i] for i in np.argsort(d)]

# Compare images
plt.close('all')
plt.figure();   plt.imshow(read_image(image_id0))
plt.figure();   plt.imshow(read_image(other_image_ids[1]))


# Render mosaic.
mesh = trimesh.load(mesh_fname)
pts = mesh.vertices.T
model_bounds = np.array([[min(pts[i]), max(pts[i])] for i in range(3)])

img_stitched = np.zeros_like(img)
xyz = np.vstack([X.ravel(), Y.ravel(), Z.ravel()])

for image_id in day2_image_ids:
    print(image_id)
    cm = std_cams[images[image_id].camera_id]
    im_pts = cm.project(xyz, t=image_id).astype(np.float32)
    mapx = np.reshape(im_pts[0], X.shape)
    mapy = np.reshape(im_pts[1], X.shape)
    radius = 50
    mask = np.logical_and(mapx > radius, mapx < cm.width - radius)
    mask = np.logical_and(mask, mapy > radius)
    mask = np.logical_and(mask, mapy < cm.height - radius)

    img_ = cv2.remap(read_image(image_id), mapx, mapy, cv2.INTER_AREA)
    img_stitched[mask] = img_[mask]
